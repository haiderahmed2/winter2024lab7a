//import.java.util.Scanner;
public class SimpleWar{
	public static void main(String[] args){
		Deck newDeck = new Deck();
		newDeck.shuffle();
		int player1 = 0;
		int player2 = 0;
		while(newDeck.length() > 1){
			Card firstCard = newDeck.drawTopCard();
			Card secondCard = newDeck.drawTopCard();
			double p1 = firstCard.calculateScore();
			double p2 = secondCard.calculateScore();
			if(p1 > p2){
			player1 = player1 + 1;
			} else {
			player2 = player2 + 1;
		}
		System.out.println(firstCard);
		System.out.println(secondCard);
		System.out.println("Player one score: " + player1);
		System.out.println("Player two score: " + player2);
		}
		if(player1 > player2){
			System.out.println("Congrats player number one for winning!");
		} else {
			System.out.println("Congrats player number two for winning!");
		}
	}
}