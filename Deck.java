import java.util.Random;
public class Deck{

    private Random rng;
    private int numberOfCards;
    private Card[] cards;

    public Deck(){
        this.rng = new Random();
        this.numberOfCards = 52;
        this.cards = new Card[52];

        //Makes an array of all the 4 suits
        String[] suits = new String[]{"Diamond", "Heart", "Club", "Spade"};

        //An array of 14 for each value A-K
        String[] values = new String[]{"A", "TWO", "THREE", "FOUR", "FIVE", "SIX", "SEVEN", "EIGHT", "NINE", "TEN", "J", "Q", "K"};

        //Loop for Diamonds
        for (int i = 0; i < values.length; i++){
            cards[i] = new Card(suits[0], values[i]);
            //System.out.println(cards[i]);
        }

        //Loop for Hearts
        for (int i = 0; i < values.length; i++){
            cards[13 + i] = new Card(suits[1], values[i]);
            //System.out.println(cards[13 + i]);
        }

        //Loop for Clubs
        for (int i = 0; i < values.length; i++){
            cards[26 + i] = new Card(suits[2], values[i]);
            //System.out.println(cards[26 + i]);
        }

        //Loop for Spades
        for (int i = 0; i < values.length; i++){
            cards[39 + i] = new Card(suits[3], values[i]);
            //System.out.println(cards[39 + i]);
        }
    }
	
	public int length(){
	//return.this.cards.length
	//Returns the number of cards
	return this.numberOfCards;
	}
    
	public Card drawTopCard(){
		//Returns the last card and removes one card from the numberOfCards
		this.numberOfCards--;
		return this.cards[this.numberOfCards];
	}
	
	public String toString(){
		
		String allCards = "";
		for(int i = 0; i < this.numberOfCards; i++){
		allCards = allCards + cards[i] + "\n";
		
		}
		
		return allCards;
		//return Integer.toString(this.numberOfCards);
	}
	
	public void shuffle(){
		for(int i = 0; i < numberOfCards - 1; i++){
			int randomIndex = rng.nextInt(numberOfCards);
			Card temp = this.cards[i];
			this.cards[i] = this.cards[randomIndex];
			this.cards[randomIndex] = temp;
		}
			
	}
}
