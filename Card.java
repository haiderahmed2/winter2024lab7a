public class Card{
	
	private String suit;
	private String value;
	
	public Card(String suit, String value){
	this.suit = suit;
	this.value = value;
	}
	
	public String getSuit(){
	return this.suit;
	}
	
	public String getValue(){
	return this.value;
	}
	
	public String toString(){
	return this.value + " of " + this.suit;
	}
	
	public double calculateScore(){
	double score = 0.0;
	//String[] values = new String[]{"A", "TWO", "THREE", "FOUR", "FIVE", "SIX", "SEVEN", "EIGHT", "NINE", "TEN", "J", "Q", "K"};
	//for(int i = 0; i < values.length; i++){
	//}
	if(value.equals("A") && suit.equals("Heart")){
		score = 1.04;
	} else if(value.equals("A") && suit.equals("Spade")) {
		score = 1.03;
	} else if(value.equals("A") && suit.equals("Diamond")) {
		score = 1.02;
	} else if(value.equals("A") && suit.equals("Club")) {
		score = 1.01;
	} else if (value.equals("TWO") && suit.equals("Heart")){
		score = 2.04;
	} else if(value.equals("TWO") && suit.equals("Spade")) {
		score = 2.03;
	} else if(value.equals("TWO") && suit.equals("Diamond")) {
		score = 2.02;
	} else if(value.equals("TWO") && suit.equals("Club")) {
		score = 2.01;
	} else if (value.equals("THREE") && suit.equals("Heart")){
		score = 3.04;
	} else if(value.equals("THREE") && suit.equals("Spade")) {
		score = 3.03;
	} else if(value.equals("THREE") && suit.equals("Diamond")) {
		score = 3.02;
	} else if(value.equals("THREE") && suit.equals("Club")) {
		score = 3.01;
	} else if (value.equals("FOUR") && suit.equals("Heart")){
		score = 4.04;
	} else if(value.equals("FOUR") && suit.equals("Spade")) {
		score = 4.03;
	} else if(value.equals("FOUR") && suit.equals("Diamond")) {
		score = 4.02;
	} else if(value.equals("FOUR") && suit.equals("Club")) {
		score = 4.01;
	} else if (value.equals("FIVE") && suit.equals("Heart")){
		score = 5.04;
	} else if(value.equals("FIVE") && suit.equals("Spade")) {
		score = 5.03;
	} else if(value.equals("FIVE") && suit.equals("Diamond")) {
		score = 5.02;
	} else if(value.equals("FIVE") && suit.equals("Club")) {
		score = 5.01;
	} else if (value.equals("SIX") && suit.equals("Heart")){
		score = 6.04;
	} else if(value.equals("SIX") && suit.equals("Spade")) {
		score = 6.03;
	} else if(value.equals("SIX") && suit.equals("Diamond")) {
		score = 6.02;
	} else if(value.equals("SIX") && suit.equals("Club")) {
		score = 6.01;
	} else if (value.equals("SEVEN") && suit.equals("Heart")){
		score = 7.04;
	} else if(value.equals("SEVEN") && suit.equals("Spade")) {
		score = 7.03;
	} else if(value.equals("SEVEN") && suit.equals("Diamond")) {
		score = 7.02;
	} else if(value.equals("SEVEN") && suit.equals("Club")) {
		score = 7.01;
	} else if (value.equals("EIGHT") && suit.equals("Heart")){
		score = 8.04;
	} else if(value.equals("EIGHT") && suit.equals("Spade")) {
		score = 8.03;
	} else if(value.equals("EIGHT") && suit.equals("Diamond")) {
		score = 8.02;
	} else if(value.equals("EIGHT") && suit.equals("Club")) {
		score = 8.01;
	} else if (value.equals("NINE") && suit.equals("Heart")){
		score = 9.04;
	} else if(value.equals("NINE") && suit.equals("Spade")) {
		score = 9.03;
	} else if(value.equals("NINE") && suit.equals("Diamond")) {
		score = 9.02;
	} else if(value.equals("NINE") && suit.equals("Club")) {
		score = 9.01;
	} else if (value.equals("TEN") && suit.equals("Heart")){
		score = 10.04;
	} else if(value.equals("TEN") && suit.equals("Spade")) {
		score = 10.03;
	} else if(value.equals("TEN") && suit.equals("Diamond")) {
		score = 10.02;
	} else if(value.equals("TEN") && suit.equals("Club")) {
		score = 10.01;
	} else if (value.equals("J") && suit.equals("Heart")){
		score = 11.04;
	} else if(value.equals("J") && suit.equals("Spade")) {
		score = 11.03;
	} else if(value.equals("J") && suit.equals("Diamond")) {
		score = 11.02;
	} else if(value.equals("J") && suit.equals("Club")) {
		score = 11.01;
	} else if (value.equals("Q") && suit.equals("Heart")){
		score = 12.04;
	} else if(value.equals("Q") && suit.equals("Spade")) {
		score = 12.03;
	} else if(value.equals("Q") && suit.equals("Diamond")) {
		score = 12.02;
	} else if(value.equals("Q") && suit.equals("Club")) {
		score = 12.01;
	} else if (value.equals("K") && suit.equals("Heart")){
		score = 13.04;
	} else if(value.equals("K") && suit.equals("Spade")) {
		score = 13.03;
	} else if(value.equals("K") && suit.equals("Diamond")) {
		score = 13.02;
	} else if(value.equals("K") && suit.equals("Club")) {
		score = 13.01;
	}
	return score;
	}
	//return score;
}